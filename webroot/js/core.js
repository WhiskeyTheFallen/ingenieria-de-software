/*------- Page Loader -------*/

 if ((".loader").length) {
      // show Preloader until the website ist loaded
      $(window).on('load', function () {
        $(".loader").fadeOut("slow");
      });
    }

$(document).ready(function() {
  $(".nav-tabs").on("click", "a", function() {
    $($(this).attr("href")).children().empty();
    $($(this).attr("href")).children().append('<table class="table"><thead><tr><th scope="col">Hora</th><th scope="col">Informacón</th><th scope="col">Acción</th></tr></thead><tbody></tbody></table>');
    var tabla=$('.table').children("tbody");
    for(var i=6; i<=23; i++){
      tabla.append('<tr>');
      tabla.append('<th>'+i+':00'+'</th>');
      tabla.append('<th>'+i+':00'+'</th>');
      tabla.append('<td height="70"><button class="btn btn-link" style="color:green;">Reservar</button></td>');
      tabla.append('</tr>');
    }
  });

  $(".tab-content").on("click", "button", function() {
    $(this).prop("disabled", true);
    $(this).css("color", "red");
    $(this).text("Reservado");
  });

});


$(document).ready(function() {
	var inputnombrecompleto=$("#nombrecompleto");
	var inputapepaterno=$("#apepaterno");
	var inputapematerno=$("#apematerno");
	var inputcontrasena=$("#contrasena");
	var inputcontrasenaconfirmar=$("#contrasenaconfirmar");
	var inputclave=$("#clave");
	var inputcorreo=$("#correo");
	var inputcurp=$("#curp");
	var inputdireccion=$("#direccion");
	var inputrfc=$("#rfc");
	var select=$("select");
	var inputfechanacimiento=$("#birthDate");
	var divError=$("#textoError");
	var spanError=divError.children();

	var boolnombrecompleto=false;
	var boolapepaterno=false;
	var boolapematerno=false;
	var boolcontrasena=false;
	var boolconfirmacontrasena=false;
	var boolcorreo=false;
	var boolclave=false;
	var boolcurp=false;
	var booldireccion=false;
	var boolselect=false;
	var boolfechanacimiento=false;
	
	// Evento select 
	select.change(function(){
		divError.prop('hidden', true);
		sexo=select.children(":selected").val();
		boolselect=true;
	});

	//Checar fecha nacimiento
	inputfechanacimiento.change(function(){
		divError.prop('hidden', true);
		fechanacimiento=inputfechanacimiento.val();
		boolfechanacimiento=true;
	});
	
	//Checar nombre completo
	inputnombrecompleto.change(function(){
		nombrecompleto=inputnombrecompleto.val();
		var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i;
	   	if(regex.test(nombrecompleto)){
			divError.prop('hidden', true);
			boolnombrecompleto=true;
	   	}
	   	else{
			boolnombrecompleto=false;
			divError.prop('hidden', false);
			spanError.text("Campo Nombre Completo inválido");
	   	}
	});

	//Checar apellido paterno
	inputapepaterno.change(function(){
		apepaterno=inputapepaterno.val();
		var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i;
	   	if(regex.test(apepaterno)){
			boolapepaterno=true;
			divError.prop('hidden', true);
	   	}
	   	else{
			boolapepaterno=false;
			divError.prop('hidden', false);
			spanError.text("Campo Apellido Paterno inválido");
	   	}
	});

	//Checar apellido materno
	inputapematerno.change(function(){
		apematerno=inputapematerno.val();
		var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i;
	   	if(regex.test(apematerno)){
			boolapematerno=true;
			divError.prop('hidden', true);
	   	}
	   	else{
			boolapematerno=false;
			divError.prop('hidden', false);
			spanError.text("Campo Apellido Materno inválido");
	   	}
	});

	//Checar contraseña
	inputcontrasena.change(function(){
		contrasena=inputcontrasena.val();
		//Asi se checa que tiene al menos una mayuscula, un numero, una minuscula y que sean 8
		var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])[a-zA-Z0-9]{8}$/i;
	   	if(regex.test(contrasena)){
			boolcontrasena=true;
			divError.prop('hidden', true);
	   	}
	   	else{
			boolcontrasena=false;
			divError.prop('hidden', false);
			spanError.text("Contraseña inválida, introduce 8 caracteres en donde haya al menos una letra mayúscula, un número y una letra minúscula");
	   	}
	});

	//Checar confirma contraseña
	inputcontrasenaconfirmar.change(function(){
		confirmacontrasena=inputcontrasenaconfirmar.val();
		if(confirmacontrasena==contrasena){
			boolconfirmacontrasena=true;
			divError.prop('hidden', true);
		}
		else{
			boolconfirmacontrasena=false;
			divError.prop('hidden', false);
			spanError.text("Las contraseñas no son iguales");
		}	
	});

	//Checa que la clave se haya dado de alta en la base de datos
	inputclave.change(function(){
		clave=inputclave.val();
		function passClave(clave){
			var data = {
				clave:clave
			};
			$.ajax({
				type: "POST",
				dataType: "json",
				data: data,
				url: 'http://aprendizajeweb.com.mx/Server/leerDBClaves.php',
				success: function(data) {
					if(data){
						boolclave=true;
						divError.prop('hidden', true);
					}
					else{
						boolconfirmacontrasena=false;
						divError.prop('hidden', false);
						spanError.text("Al parecer la clave que introdujiste no ha sido dado de alta, acude al modulo del gimnasio para que te otorguen una");
					}
				}
			});
		}
		passClave(clave);
	});

	//Checar correo
	inputcorreo.change(function(){
		correo=inputcorreo.val();
		var regex = /^[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}$/i;
	   	if(regex.test(correo)){
			boolcorreo=true;
			divError.prop('hidden', true);
	   	}
	   	else{
			boolcorreo=false;
			divError.prop('hidden', false);
			spanError.text("Correo inválido");
	   	}
	});

	//Checa curp y lo pone ademas en RFC
	inputcurp.change(function(){
		curp=inputcurp.val();
		var regex = /^[A-ZÑ&]{3,4}\d{6}[A-Z]{6}\d{2}(?:[A-Z\d]{3})?$/i;
	   	if(regex.test(curp)){
			boolcurp=true;
			rfc=curp.slice(0, 10);
			inputrfc.val(rfc);
			divError.prop('hidden', true);
	   	}
	   	else{
			boolcurp=false;
			divError.prop('hidden', false);
			spanError.text("CURP inválido");
	   	}
	});

	//En la direccion solo checa que no haya caracteres raros
	inputdireccion.change(function(){
		direccion=inputdireccion.val();
		var regex = /^[A-Za-z0-9'\.\-\s\,]+$/i;
	   	if(regex.test(direccion)){
			booldireccion=true;
			divError.prop('hidden', true);
	   	}
	   	else{
			booldireccion=false;
			divError.prop('hidden', false);
			spanError.text("Dirección inválida");
	   	}
	});

	$("#submit").click(function(){
		if(!boolnombrecompleto){
			divError.prop('hidden', false);
			spanError.text("Campo Nombre Completo inválido");
		}
		else if(!boolapepaterno){
			divError.prop('hidden', false);
			spanError.text("Campo Apellido Paterno inválido");
		}
		else if(!boolapematerno){
			divError.prop('hidden', false);
			spanError.text("Campo Apellido Materno inválido");
		}
		else if(!boolcontrasena){
			divError.prop('hidden', false);
			spanError.text("Campo Contraseña inválido");
		}
		else if(!boolconfirmacontrasena){
			divError.prop('hidden', false);
			spanError.text("Campo Confirma contraseña inválido");
		}
		else if(!boolcorreo){
			divError.prop('hidden', false);
			spanError.text("Campo Clave inválido");
		}
		else if(!boolcorreo){
			divError.prop('hidden', false);
			spanError.text("Campo Correo inválido");
		}
		else if(!boolcurp){
			divError.prop('hidden', false);
			spanError.text("Campo CURP inválido");
		}
		else if(!booldireccion){
			divError.prop('hidden', false);
			spanError.text("Campo Dirección inválido");
		}
		else if(!boolselect){
			divError.prop('hidden', false);
			spanError.text("Seleccione su sexo");
		}
		else if(!boolfechanacimiento){
			divError.prop('hidden', false);
			spanError.text("Introduzca su fecha de nacimiento");
		}
		else{
			function passUsuario(){
				var data = {
					nombrecompleto: nombrecompleto,
					apepaterno: apepaterno,
					apematerno: apematerno,
					contrasena: contrasena,
					correo: correo,
					curp: curp,
					rfc: rfc,
					direccion: direccion,
					sexo: sexo,
					fechanacimiento: fechanacimiento
				};
				$.post("http://aprendizajeweb.com.mx/Server/escribirDBUsuarios.php", data, function(data){
					
					if(data){
						window.location.href = "../src/inicio.html";
					}
					else{
						divError.prop('hidden', false);
						spanError.text("Error, usuario existente en el sistema");
					}
				});
			}
			passUsuario();

		}
		
	});
});
